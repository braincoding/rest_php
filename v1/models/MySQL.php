<?php

namespace models;

class MySQL
{
    private $db;
    private static $instance;

    private function __construct() {
        $this->db = new \mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DB);
        if ($this->db->connect_error) {
            die('Connect error (' . $this->db->connect_errno . ') ' . $this->db->connect_error);
        }
        $this->db->set_charset('utf8');
    }

    protected function __clone() {}

    static public function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function insert($table, $data) {
        $columns = array();
        $values = array();

        foreach ($data as $key => $value) {
            $key = $this->db->real_escape_string($key);
            $columns[] = $key;

            if ($value == NULL) {
                $values[] = 'NULL';
            } else {
                $value = $this->db->real_escape_string($value);
                $values[] = "'$value'";
            }
        }

        $columns = implode(',', $columns);
        $values = implode(',', $values);

        $sql = "INSERT INTO $table ($columns) VALUES ($values)";
        $res = $this->db->query($sql);

        if (!$res) {
            die('Insert error (' . $this->db->errno . ') ' . $this->db->error);
        }

        return $this->db->insert_id;
    }

    public function select($sql) {
        $res = $this->db->query($sql);
        if ($this->db->errno) {
            die('Select error (' . $this->db->errno . ') ' . $this->db->error);
        }

        $data = array();
        for ($i=0; $i < $res->num_rows; $i++) { 
            $data[] = $res->fetch_assoc();
        }

        return $data;
    }

    public function update($table, $data, $where) {
        $set = array();

        foreach ($data as $key => $value) {
            $key = $this->db->real_escape_string($key);

            if ($value == NULL) {
                $set[] = "$key=NULL";
            } else {
                $value = $this->db->real_escape_string($value);
                $set[] = "$key='$value'";
            }
        }

        $set = implode(',', $set);

        $sql = "UPDATE $table SET $set WHERE $where";
        $res = $this->db->query($sql);

        if (!$res) {
            die('Update error (' . $this->db->errno . ') ' . $this->db->error);
        }

        return $this->db->affected_rows;
    }

    public function delete($table, $where) {
        $sql = "DELETE FROM $table WHERE $where";
        $res = $this->db->query($sql);

        if (!$res) {
            die('Delete error (' . $this->db->errno . ') ' . $this->db->error);
        }

        return $this->db->affected_rows;
    }
}
