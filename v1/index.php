<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);

require_once 'config.php';
require_once 'autoloader.php';

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, DELETE');

if (!isset($_SERVER['REQUEST_URI'])) {
    $dir = basename(__DIR__);
    $path = parse_url($_SERVER['REQUEST_URI']);
    $path = $path['path'];
    $_SERVER['PATH_INFO'] = substr($path, strpos($path, $dir) + strlen($dir));
}

// создаем объект запроса
$request = models\Request::getInstance();
if (!$request->isValid()) {
    header('HTTP/1.1 400 Bad request');
    die;
}

// проверяем наличие контроллера
$controllerName = $request->getPath();
$controllerName = 'controllers\\' . ucfirst($controllerName[0]) . 'Controller';
if (!class_exists($controllerName)) {
    header('HTTP/1.1 400 Bad request');
    die;
}

// создаем котроллер
$controller = new $controllerName();

// выполняем действие
$actionName = $request->getMethod() . 'Action';
$result = $controller->$actionName($request->getPath(), $request->getParams());

// проверяем результат
if ($result === false) {
    header('HTTP/1.1 400 Bad request');
    die;
}

// отдаем данные в формате JSON
header('Content-Type: application/json');
echo json_encode($result);
