<?php

namespace controllers;

class BaseController
{
    public function __call($name, $arguments) {
        header('HTTP/1.1 405 Method Not Allowed');
        die;
    }
}
