<?php

namespace controllers;

use repository;

class KeywordsController extends BaseController
{
    private $repository;
	private $repPersons;

    public function __construct() {
        $this->repository = new repository\KeywordsRepository();
        $this->repPersons = new repository\PersonsRepository();
    }

    public function postAction($path, $params) {
        if (!isset($params['person_id']) || $params['person_id'] == '') return false;
        if (!isset($params['name']) || $params['name'] == '') return false;
		
		$res = $this->repPersons->get($params['person_id']);
		if (count($res) == 0) return false;

        $data = array(
            'name' => $params['name'],
            'person_id' => $params['person_id']
        );

        return $this->repository->add($data);
    }

    public function getAction($path, $params) {
        if (isset($params['person_id'])) {
            return $this->repository->find(array('person_id' => $params['person_id']));
        }

        return $this->repository->getAll();
    }

    public function deleteAction($path, $params) {
        if (!isset($path[1]) || !is_numeric($path[1])) return false;

        return $this->repository->remove($path[1]);
    }
}
