<?php

namespace controllers;

use repository;

class SitesController extends BaseController
{
    private $repository;

    public function __construct() {
        $this->repository = new repository\SitesRepository();
    }

    public function postAction($path, $params) {
        if (!isset($params['name']) || $params['name'] == '') return false;

        $data = array(
            'name' => $params['name']
        );

        return $this->repository->add($data);
    }

    public function getAction($path, $params) {
        return $this->repository->getAll();
    }

    public function deleteAction($path, $params) {
        if (!isset($path[1]) || !is_numeric($path[1])) return false;

        return $this->repository->remove($path[1]);
    }
}
