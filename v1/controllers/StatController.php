<?php

namespace controllers;

use repository;

class StatController extends BaseController
{
    private $repPersons;
    private $repPages;
    private $repRanks;
    private $repSites;

    public function __construct() {
        $this->repPersons   = new repository\PersonsRepository();
        $this->repPages     = new repository\PagesRepository();
        $this->repRanks     = new repository\RanksRepository();
        $this->repSites     = new repository\SitesRepository();
    }

    public function getAction($path, $params) {
        if (!isset($path[1])) return false;

        switch ($path[1]) {
            case 'common':
                return $this->getCommonStats($params);
            case 'daily':
                return $this->getDailyStats($params);
        }

        return false;
    }

    private function getCommonStats($params) {
        if (!isset($params['site_id'])) return false;

        $persons = $this->repPersons->getAll();
        $pages = $this->repPages->find(array('site_id' => $params['site_id']));
        $ranks = $this->repRanks->getAll();
        $res = array();

        foreach ($persons as $person) {
            $personRank = 0;

            foreach ($pages as $page) {
                foreach ($ranks as $rank) {
                    if ($rank['person_id'] == $person['id'] && $rank['page_id'] == $page['id']) {
                        $personRank += $rank['rank'];
                    }
                }
            }

            $res[] = array(
                'id'    => $person['id'],
                'name'  => $person['name'],
                'rank'  => $personRank
            );
        }

        return $res;
    }

    private function getDailyStats($params) {
        if (!isset($params['site_id'])) return false;
        if (!isset($params['person_id'])) return false;

        if (!isset($params['first_date']) || !isset($params['last_date'])) {
            $params['first_date'] = date('Y-m-d', time() - DAY_IN_SECONDS * 30);
            $params['last_date'] = date('Y-m-d', time());
        }

        $params['first_date'] = strtotime($params['first_date']);
        $params['last_date'] = strtotime($params['last_date']);

        if ($params['first_date'] > $params['last_date']) {
            return false;
        }

        $ranks = $this->repRanks->find(array('person_id' => $params['person_id']));
        $pages = $this->repPages->find(array('site_id' => $params['site_id']));
        $res = array('pagesByDays' => array(), 'totalPages' => 0);

        $date = $params['first_date'];
        do {
            $newPages = 0;

            foreach ($ranks as $rank) {
                foreach ($pages as $page) {
                    $pageDate = strtotime($page['found_date_time']);
                    $pageDate = date('Y-m-d', $pageDate);
                    $pageDate = strtotime($pageDate);
                    if ($pageDate == $date && $rank['page_id'] == $page['id']) {
                        $newPages++;
                    }
                }
            }

            $res['pagesByDays'][] = array(
                'date' => date('d-m-Y', $date),
                'pages' => $newPages
            );
            $res['totalPages'] += $newPages;

            $date += DAY_IN_SECONDS;
        } while ($date < $params['last_date']);

        return $res;
    }
}
