<?php

namespace repository;

use models;

class Repository implements IRepository
{
    protected $db;
    protected $key;
    protected $table;

    public function __construct($table, $key = 'id') {
        $this->db = models\MySQL::getInstance();
        $this->table = $table;
        $this->key = $key;
    }

    public function add($data) {
        return $this->db->insert($this->table, $data);
    }

    public function get($id) {
        return $this->db->select("SELECT * FROM {$this->table} WHERE {$this->key}=$id");
    }

    public function getAll() {
        return $this->db->select("SELECT * FROM {$this->table}");
    }

    public function remove($id) {
        return $this->db->delete($this->table, "{$this->key}=$id");
    }

    public function find($filter) {
        $where = array();

        foreach ($filter as $key => $value) {
            if ($value == NULL) {
                $where[] = "$key=NULL";
            } else {
                $where[] = "$key='$value'";
            }
        }

        $where = implode(' AND ', $where);
        return $this->db->select("SELECT * FROM {$this->table} WHERE $where");
    }
}
