<?php

namespace repository;

interface IRepository
{
    public function add($data);
    public function get($id);
    public function getAll();
    public function remove($id);
    public function find($filter);
}
